// tslint:disable:max-classes-per-file

/**
 * COMMON
 */

// Types
type StrictMap<K extends string, V> = { [key in K]: V };
type Map<V> = StrictMap<string, V>;
type Falsey = false | undefined | null | '' | 0;
type Range = { lower: number; upper: number };

// Verifiers
export const isDefined = (o: any, t: string) => o && typeof o === t;

export const optional = <T>(o: any, v: (o: any) => o is T): o is T | Falsey => !o || v(o);

export const optionalIf = <T>(c: boolean, o: any, v: (o: any) => o is T): o is T | Falsey =>
  c ? optional(o, v) : v(o);

export const isString = (o: any): o is string => isDefined(o, 'string');

export const isNumber = (o: any): o is number => isDefined(o, 'number');

export const isBoolean = (o: any): o is boolean => o === true || o === false;

export const isArray = <T>(o: any, v: (o: any) => o is T): o is T[] => Array.isArray(o) && o.every(e => v(e));

export const isPair = <T>(o: any, v: (o: any) => o is T): o is [T, T] =>
  Array.isArray(o) && o.length === 2 && v(o[0]) && v(o[1]);

export const isStrictTuple = (o: any, ...choices: any[][]) =>
  Array.isArray(o) && o.length === choices.length && choices.every((e, i) => e.includes(o[i]));

export const isAny = (o: any): o is any => true;

export const isRange = (o: any): o is Range => isArray([o.lower, o.upper], isNumber);

export const isMap = <T>(o: any, v: (o: any) => o is T): o is Map<T> => {
  if (!o || typeof o !== 'object') return false;

  return Object.keys(o).every(k => v(o[k]));
};

export const makeChecker = <T, U>(checker: (o: any, v: (o: any) => o is U) => o is T, v: (o: any) => o is U) => {
  return ((o: any) => {
    return checker(o, v);
  }) as (o: any) => o is T;
};

/**
 * INPUT INTERFACES
 */

// LOGIN & AUTHENTICATION
const LoginTypes = ['ADMIN', 'TEACHER', 'USER'] as const;

export type LoginType = typeof LoginTypes[number];

export function isLoginType(o: any): o is LoginType {
  return isString(o) && LoginTypes.includes(o as LoginType);
}

export interface ITokenForm {
  loginType: LoginType;
}

export function isTokenForm(o: any): o is ITokenForm {
  return o && isLoginType(o.loginType);
}

export interface ILoginForm extends ITokenForm {
  email: string;
  password: string;
}

export function isLoginForm(o: any): o is ILoginForm {
  return isString(o.email) && isString(o.password) && isTokenForm(o);
}

// ACCOUNT CREATION
export interface ICreateAccount {
  email: string;
  fname: string;
  lname: string;
  profilepic?: string;
}

export function isCreateAccount(o: any): o is ICreateAccount {
  return o && isString(o.email) && isString(o.fname) && isString(o.lname) && optional(o.profilepic, isString);
}

export interface IUpdateAccount {
  gender?: 'm' | 'f';
  phone?: string;
  orientation?: string;
  profilepic?: string;
  password?: string;
}

const IUPDATE_ACCOUNT_KEYS = ['profilepic', 'password', 'gender', 'orientation', 'phone'];

export function isUpdateAccount(o: any): o is IUpdateAccount {
  return (
    o &&
    optional(o.profilepic, isString) &&
    optional(o.password, isString) &&
    optional(o.gender, isString) &&
    optional(o.phone, isString) &&
    optional(o.orientation, isString) &&
    Object.keys(o).every(k => IUPDATE_ACCOUNT_KEYS.includes(k))
  );
}

// GROUP CREATION
const USER_GROUP_TYPES = ['L', 'C'] as const;
export type UserGroupType = typeof USER_GROUP_TYPES[number];

export function isUserGroupType(o: any): o is UserGroupType {
  return isString(o) && USER_GROUP_TYPES.includes(o as UserGroupType);
}

const USER_GROUP_CREATION_TYPES = ['AUTO', 'MANUAL'] as const;
export type UserGroupCreationType = typeof USER_GROUP_CREATION_TYPES[number];

export function isUserGroupCreationType(o: any): o is UserGroupCreationType {
  return isString(o) && USER_GROUP_CREATION_TYPES.includes(o as UserGroupCreationType);
}

export interface IUserGroupSettings {
  nbUsers: Range;
  algorithm: SuggestionAlgorithm;
  creationType: UserGroupCreationType;
}

export function isUserGroupSettings(o: any): o is IUserGroupSettings {
  return o && isRange(o.nbUsers) && isSuggestionAlgorithm(o.algorithm) && isUserGroupCreationType(o.creationType);
}

type UserGroupAcronym = string;

export function isUserGroupAcronym(o: any): o is UserGroupAcronym {
  return isString(o) && /^[A-Z]{3}[0-9]{4}([A-Z]{1})?_[0-9]{2}_[lc]$/.test(o);
}

export interface ICreateUserGroup {
  acronym: UserGroupAcronym;
  users: ICreateAccount[];
  settings: IUserGroupSettings;
}

export function isCreateUserGroup(o: any): o is ICreateUserGroup {
  return o && isArray(o.users, isCreateAccount) && isUserGroupAcronym(o.acronym) && isUserGroupSettings(o.settings);
}

// SUGGESTION ALFORITHMS
const SUGGESTION_ALGORITHMS = ['RANDOM', 'MBTI', 'ORDERED'] as const;

export type SuggestionAlgorithm = typeof SUGGESTION_ALGORITHMS[number];

export function isSuggestionAlgorithm(o: any): o is SuggestionAlgorithm {
  return isString(o) && SUGGESTION_ALGORITHMS.includes(o as SuggestionAlgorithm);
}

export interface ISuggestionForm {
  acronym: string;
}

export function isSuggestionForm(o: any): o is ISuggestionForm {
  return o && isString(o.acronym);
}

// MBTI
export interface MBTIAnswer {
  id: number;
  answer: 'a1' | 'a2';
}

export function isMBTIAnswer(o: any): o is MBTIAnswer {
  return o && isNumber(o.id) && isString(o.answer) && ['a1', 'a2'].includes(o.answer);
}

export interface MBTIAnswerFormPart {
  id: number;
  answers: MBTIAnswer[];
}

export function isMBTIAnswerFormPart(o: any): o is MBTIAnswerFormPart {
  return o && isNumber(o.id) && isArray(o.answers, isMBTIAnswer);
}

export type MBTIAnswerForm = MBTIAnswerFormPart[];

export function isMBTIAnswerForm(o: any): o is MBTIAnswerForm {
  return isArray(o, isMBTIAnswerFormPart);
}

// TEAM CREATION

const LIKE_FACTORS = [-2, -1, 1, 2] as const;

export type LikeFactor = typeof LIKE_FACTORS[number];

export function isLikeFactor(o: any): o is LikeFactor {
  return o && LIKE_FACTORS.includes(o);
}

export interface IUserLike {
  courseId: string;
  userEmail: string;
  likeFactor: LikeFactor;
}

export function isUserLike(o: any): o is IUserLike {
  return o && isArray([o.courseId, o.userEmail], isString) && isLikeFactor(o.likeFactor);
}

export interface IGetTeam {
  courseId: string;
}

export function isGetTeam(o: any): o is IGetTeam {
  return o && isString(o.courseId);
}

export interface IUpdateTeams extends IGetTeam {
  teams: ICourseTeam[];
}

export function isUpdateTeams(o: any): o is IUpdateTeams {
  return o && isArray(o.teams, isCourseTeam) && isGetTeam(o);
}

export interface IJoinCode {
  code: string;
}

export function isJoinCode(o: any): o is IJoinCode {
  return o && isString(o.code);
}

export interface IBackup {
  key: string;
}

export function isBackup(o: any): o is IBackup {
  return o && isString(o.key);
}

/**
 * OUTPUT INTERFACES
 */

// GENERAL
export interface IServerResponse<T> {
  code: number;
  msg?: string;
  data?: T;
}

export function makeResponse<T>(code: number, data?: T, msg?: string): IServerResponse<T> {
  return { code, msg, data };
}

export function isServerResponse<T>(o: any, isT: (t: any) => t is T): o is IServerResponse<T> {
  return o && isNumber(o.code) && optional(o.msg, isString) && optional(o.data, isT);
}

// LOGIN & USER FETCHING
export class IUserPublicInfo {
  email: string = '';
  fname: string = '';
  lname: string = '';
  profilepic?: string = '';
  personality?: PersonalityType = undefined;
}

export function isUserPublicInfo(o: any): o is IUserPublicInfo {
  return (
    o &&
    isArray([o.email, o.fname, o.lname], isString) &&
    optional(o.profilepic, isString) &&
    optional(o.personality, isPersonalityType)
  );
}

export class IUserPersonalInfo extends IUserPublicInfo {
  type: LoginType = '' as LoginType;
}

export function isUserPersonalInfo(o: any): o is IUserPersonalInfo {
  return o && isLoginType(o.type) && isUserPublicInfo(o);
}

export interface IUserInfoCourse {
  courseId: string;
  locked: boolean;
  completedProfiles?: number;
}

export function isUserInfoCourse(o: any): o is IUserInfoCourse {
  return o && isString(o.courseId) && isBoolean(o.locked) && optional(o.completedProfiles, isNumber);
}

export class IUserInfo extends IUserPublicInfo {
  courses: IUserInfoCourse[] = [];
  jwt: string = '';
  logintype!: LoginType;
  codes: Map<string> = {};
}

export function isUserInfo(o: any): o is IUserInfo {
  return (
    o &&
    isArray(o.courses, isUserInfoCourse) &&
    isString(o.jwt) &&
    isLoginType(o.logintype) &&
    isMap(o.codes, isString) &&
    isUserPublicInfo(o)
  );
}

const isUserPublicInfoArray = makeChecker(isArray, isUserPublicInfo);

// MBTI

export type PersonalityType = ['E' | 'I', 'S' | 'N', 'T' | 'F', 'J' | 'P'];

export function isPersonalityType(o: any): o is PersonalityType {
  return o && isStrictTuple(o, ['E', 'I'], ['S', 'N'], ['T', 'F'], ['J', 'P']);
}

export interface MBTIQuestion {
  id: number;
  question: string | null;
  a1: string;
  a2: string;
}

export function isMBTIQuestion(o: any): o is MBTIQuestion {
  return o && isNumber(o.id) && optional(o.question, isString) && isArray([o.a1, o.a2], isString);
}

export interface MBTIFormPart {
  id: number;
  title: string;
  questions: MBTIQuestion[];
}

export function isMBTIFormPart(o: any): o is MBTIFormPart {
  return o && isNumber(o.id) && isString(o.title) && isArray(o.questions, isMBTIQuestion);
}

export type MBTIForm = MBTIFormPart[];

export function isMBTIForm(o: any): o is MBTIForm {
  return o && isArray(o, isMBTIFormPart);
}

// TEAM CREATION

export interface ITeamCreationResponse {
  matched: boolean;
}

export function isTeamCreationResponse(o: any): o is ITeamCreationResponse {
  return o && isBoolean(o.matched);
}

export interface ICourseTeamUser {
  fname: string;
  lname: string;
  email: string;
  validated: boolean;
}

export function isCourseTeamUser(o: any): o is ICourseTeamUser {
  return o && isString(o.email) && isBoolean(o.validated);
}

export interface ICourseTeam {
  users: ICourseTeamUser[];
  ready: boolean;
}

export function isCourseTeam(o: any): o is ICourseTeam {
  return o && isArray(o.users, isCourseTeamUser) && isBoolean(o.ready);
}

/**
 *  ROUTES
 */

export type RouteMethod = 'get' | 'post' | 'put' | 'delete';

export class RouteVerifier<I, O> {
  url: string;
  method: RouteMethod;
  in: (o: any) => o is I;
  out: (o: any) => o is IServerResponse<O>;

  public constructor(url: string, method: RouteMethod, inp: (o: I) => o is I, out: (o: O) => o is O) {
    this.url = url;
    this.method = method;
    this.in = inp;
    this.out = makeChecker(isServerResponse, out);
  }
}

const ROUTE_KEYS = [
  'login',
  'token',
  'suggestion',
  'mbtiForm',
  'mbtiProcess',
  'admin',
  'teacher',
  'teacherList',
  'upload',
  'updateUser',
  'likeUser',
  'getTeam',
  'getTeams',
  'updateTeams',
  'createCode',
  'joinCode',
  'validateTeam',
  'createTeams',
  'listAllUsers',
  'reset',
] as const;

export type RouteKey = typeof ROUTE_KEYS[number];

type RV<I, O> = RouteVerifier<I, O>;

export abstract class IRoutes implements StrictMap<RouteKey, RouteVerifier<any, any>> {
  login!: RV<ILoginForm, IUserInfo>;
  token!: RV<ITokenForm, IUserInfo>;
  suggestion!: RV<ISuggestionForm, IUserPublicInfo[][]>;
  mbtiForm!: RV<any, MBTIForm>;
  mbtiProcess!: RV<MBTIAnswerForm, PersonalityType>;
  admin!: RV<ICreateAccount, any>;
  teacher!: RV<ICreateAccount, any>;
  teacherList!: RV<ICreateUserGroup, any>;
  upload!: RV<any, string>;
  updateUser!: RV<IUpdateAccount, string>;
  likeUser!: RV<IUserLike, ITeamCreationResponse>;
  getTeam!: RV<IGetTeam, ICourseTeam>;
  getTeams!: RV<IGetTeam, ICourseTeam[]>;
  updateTeams!: RV<IUpdateTeams, any>;
  createCode!: RV<IGetTeam, any>;
  joinCode!: RV<IJoinCode, any>;
  validateTeam!: RV<IGetTeam, any>;
  createTeams!: RV<IGetTeam, any>;
  listAllUsers!: RV<any, IUserPersonalInfo[]>;
  reset!: RV<IBackup, any>;
}

export class Routes implements IRoutes {
  login = new RouteVerifier('/login', 'post', isLoginForm, isUserInfo);
  token = new RouteVerifier('/token', 'post', isTokenForm, isUserInfo);
  suggestion = new RouteVerifier('/suggestion', 'post', isSuggestionForm, makeChecker(isArray, isUserPublicInfoArray));
  mbtiForm = new RouteVerifier('/mbti/form', 'get', isAny, isMBTIForm);
  mbtiProcess = new RouteVerifier('/mbti/process', 'post', isMBTIAnswerForm, isPersonalityType);
  admin = new RouteVerifier('/admin', 'post', isCreateAccount, isAny);
  teacher = new RouteVerifier('/teacher', 'post', isCreateAccount, isAny);
  teacherList = new RouteVerifier('/teacher/list', 'post', isCreateUserGroup, isAny);
  upload = new RouteVerifier('/upload', 'post', isAny, isString);
  updateUser = new RouteVerifier('/update/user', 'post', isUpdateAccount, isString);
  likeUser = new RouteVerifier('/like/user', 'post', isUserLike, isTeamCreationResponse);
  getTeam = new RouteVerifier('/get/team', 'post', isGetTeam, isCourseTeam);
  getTeams = new RouteVerifier('/get/teams', 'post', isGetTeam, makeChecker(isArray, isCourseTeam));
  updateTeams = new RouteVerifier('/update/teams', 'post', isUpdateTeams, isAny);
  createCode = new RouteVerifier('/create/code', 'post', isGetTeam, isAny);
  joinCode = new RouteVerifier('/join/code', 'post', isJoinCode, isAny);
  validateTeam = new RouteVerifier('/validate/team', 'post', isGetTeam, isAny);
  createTeams = new RouteVerifier('/create/teams', 'post', isGetTeam, isAny);
  listAllUsers = new RouteVerifier('/list/all', 'get', isAny, makeChecker(isArray, isUserPersonalInfo));
  reset = new RouteVerifier('/reset', 'post', isBackup, isAny);
}

export const ROUTES: IRoutes = new Routes();
